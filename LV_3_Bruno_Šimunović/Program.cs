﻿using System;

namespace LV_3_Bruno_Šimunović
{
    public enum Category { ERROR, ALERT, INFO }
    class Program
    {
        static void Main(string[] args)
        {
            Dataset prvi = new Dataset("csv.csv");
            Dataset copPrvi = (Dataset)prvi.Clone();

            RandomMatrixGenerator matrix = RandomMatrixGenerator.GetInstance();
            matrix.GetSizeOfMatrix(5, 6);
            matrix.NextRandomMatrix();
            matrix.printMatrix();

            Logger log = Logger.GetInstance();
            log.Log("Meska");

            ConsoleNotification Log = new ConsoleNotification("Bruno Šimunović", "Zelena", "Bilo jednom davno", DateTime.Now, Category.ERROR, ConsoleColor.DarkGreen);
            NotificationManager notification = new NotificationManager();
            notification.Display(Log);


        }
    }
}
