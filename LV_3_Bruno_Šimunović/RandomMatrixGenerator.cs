﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Bruno_Šimunović
{
    class RandomMatrixGenerator
    {
        private static RandomMatrixGenerator instance;
        private double[,] matrix;

        private RandomMatrixGenerator()
        {
            this.matrix = new double[0, 0];
        }
        public static RandomMatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomMatrixGenerator();
            }
            return instance;
        }

        public void GetSizeOfMatrix(int row, int column)
        {
            this.matrix = new double[row, column];
        }
        public void NextRandomMatrix()
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Random random = new Random();
                    this.matrix[i, j] = random.NextDouble() * 1;
                }
            }
        }

        public void printMatrix()
        {
            int i, j;
            for (i = 0; i < matrix.GetLength(0); i++)
            {
                for (j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
