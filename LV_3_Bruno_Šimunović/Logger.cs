﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Bruno_Šimunović
{
    class Logger
    {
        private static Logger instance;
        private String filePath;

        private Logger()
        {
            this.filePath = "text.txt";
        }

        public String FilePath { set { filePath = value; } }
       
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath, true))
            {
                writer.WriteLine(message);
                
            }
        }
    }
}
