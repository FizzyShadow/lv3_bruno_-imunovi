﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV_3_Bruno_Šimunović
{
    interface Prototype
    {
        public Prototype Clone();
    }
}
